package stringtest;

import java.lang.StringBuilder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.jupiter.api.Assertions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.util.ParameterService;

public class Steps {

    @Given("Variable {string} is set to {string}")
    public void stringIsSetTo(final String key,
                              final String str) {
        StringPool.set(key, str);
    }

    @Given("Variable {string} is set to Squash parameter {string}")
    public void stringIsSetToSquashParam(final String key,
                                         final String squashParamName) throws ParameterException {
        final String paramValue = ParameterService.INSTANCE.getString(squashParamName);
        StringPool.set(key, paramValue);
    }

    @Given("Variable {string} is set to environment variable {string}")
    public void stringIsSetToEnvironmentVariable(final String key,
                                                 final String varEnvName) throws ParameterException {
        final String paramValue =  System.getenv(varEnvName);
        StringPool.set(key, paramValue);
    }

    @Given("Variable {string} contains the dump of all environment variables")
    public void stringIsSetToEnvironmentVariableDump(final String key) throws ParameterException {
        final Map<String, String> envMap = System.getenv();
        dumpMapInVariable(key, envMap);
    }

    @Given("Variable {string} contains the dump of all system properties")
    public void stringIsSetToSystemPropertyDump(final String key) throws ParameterException {
        final Properties properties = System.getProperties();
        final Map<String, String> systemProperties = new HashMap<>();
        for (final Object k : properties.keySet()) {
            final String propertyName = (String) k;
            final String propertyValue = properties.getProperty(propertyName);
            systemProperties.put(propertyName, propertyValue);
        }
        dumpMapInVariable(key, systemProperties);
    }

    private void dumpMapInVariable(final String key,
                                   final Map<String, String> envMap) {
        final StringBuilder builder = new StringBuilder();
        for (final String envName : envMap.keySet()) {
            final String envValue = envMap.get(envName);
            if (builder.length() > 0) {
                builder.append('\n');
            }
            builder.append(envName).append("=").append(envValue);
        }
        StringPool.set(key, builder.toString());
    }

    @When("Variable {string} is reverted")
    public void stringIsReverted(final String key) {
        final String str = StringPool.get(key);
        final String rev = new StringBuilder(str).reverse().toString();
        StringPool.set(key, rev);
    }

    @When("Variable {string} is uppercased")
    public void stringIsUppercased(final String key) {
        final String str = StringPool.get(key);
        final String rev = str.toUpperCase();
        StringPool.set(key, rev);
    }

    @When("Variable {string} is the concatenation of variable {string} and variable {string} joined with {string}")
    public void stringIsTheConcatenationOfStringAndStringJoinedWith(final String key,
                                                                    final String key1,
                                                                    final String key2,
                                                                    final String separator) {
        final String str1 = StringPool.get(key1);
        final String str2 = StringPool.get(key2);
        StringPool.set(key, str1 + separator + str2);
    }

    @When("Variable {string} is the concatenation of variable {string}, variable {string}, and variable {string} joined with {string}")
    public void stringIsTheConcatenationOfStringStringAndStringJoinedWith(final String key,
                                                                          final String key1,
                                                                          final String key2,
                                                                          final String key3,
                                                                          final String separator) {
        final String str1 = StringPool.get(key1);
        final String str2 = StringPool.get(key2);
        final String str3 = StringPool.get(key3);
        StringPool.set(key, str1 + separator + str2 + separator + str3);
    }

    @When("Variable {string} is the concatenation of variable {string}, variable {string}, variable {string}, and variable {string} joined with {string}")
    public void stringIsTheConcatenationOfStringStringStringAndStringJoinedWith(final String key,
                                                                                final String key1,
                                                                                final String key2,
                                                                                final String key3,
                                                                                final String key4,
                                                                                final String separator) {
        final String str1 = StringPool.get(key1);
        final String str2 = StringPool.get(key2);
        final String str3 = StringPool.get(key3);
        final String str4 = StringPool.get(key4);
        StringPool.set(key, str1 + separator + str2  + separator + str3 + separator + str4);
    }
    
    @Then("Variables {string} and {string} are equal")
    public void stringsAreEqual(final String key1,
                                final String key2) {
        final String str1 = StringPool.get(key1);
        final String str2 = StringPool.get(key2);
        Assertions.assertTrue(str1.equals(str2), "the value of variable \"" + key1 + "\" (\"" + str1 + "\") is not equal to the value of variable \"" + key2 + "\" (\"" + str2 + "\").");
    }

    @Then("Variable {string} is equal to {string}")
    public void stringIsEqualTo(final String key,
                                final String value) {
        final String str = StringPool.get(key);
        Assertions.assertTrue(str.equals(value), "the value of variable \"" + key + "\" (\"" + str + "\") is not equal to \"" + value + "\".");
    }
}
