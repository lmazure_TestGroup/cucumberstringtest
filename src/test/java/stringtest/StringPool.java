package stringtest;

import java.util.HashMap;
import java.util.Map;

public class StringPool {
    
    private static final Map<String, String> _map = new HashMap<String, String>();

    public static void set(final String key,
                           final String value) {
        _map.put(key, value);
    }

    public static String get(final String key) {
        return _map.get(key);
    }
}
