#language: en

Feature: Variables are revertable

Scenario: a variable can be inverted
Given Variable "a" is set to "azerty"
And   Variable "b" is set to "ytreza"
When  Variable "a" is reverted
Then  Variables "a" and "b" are equal
