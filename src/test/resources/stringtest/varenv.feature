#language: en

Feature: Environment variables are retrievable

Scenario: an environment variable can be retrieved
When Variable "a" is set to environment variable "SYSTEMDRIVE"
Then Variable "a" is equal to "C:"
