#language: en

Feature: Variables are uppercasable

Scenario: a variable can be uppercased
Given Variable "a" is set to "azerty"
And   Variable "b" is set to "AZERTY"
When  Variable "a" is uppercased
Then  Variables "a" and "b" are equal
