#language: en

Feature: Squash parameters can be retrieved

Scenario: a string can be inverted
Given Variable "1" is set to Squash parameter "TC_CUF_C01"
Given Variable "2" is set to Squash parameter "TC_CUF_C02"
Given Variable "3" is set to Squash parameter "TC_CUF_C03"
Given Variable "4" is set to Squash parameter "TC_CUF_C04"
When  Variable "X" is the concatenation of variable "1", variable "2", variable "3", and variable "4" joined with "¤"
Then  Variable "X" is equal to "dummy 2"
