# CucumberStringTest


Simplistic Cucumber tests to play with OTF (see https://autom-devops-en.doc.squashtest.com/latest/autom/techno/cucumber.html)

The supported actions are:
- Given
    - "Variable {varName} is set to {value}"
    - "Variable {varName} is set to Squash parameter {paramName}"
    - "Variable {varName} is set to environment variable {varEnvName}"
    - "Variable {varName} contains the dump of all environment variables"
    - "Variable {varName} contains the dump of all system properties"

- When
    - "Variable {varName} is reverted"
    - "Variable {varName} is uppercased"
    - "Variable {varName} is the concatenation of variable {varName} and variable {varName} joined with {value}"
    - "Variable {varName} is the concatenation of variable {varName}, variable {varName}, and variable {varName} joined with {value}"
    - "Variable {varName} is the concatenation of variable {varName}, variable {varName}, variable {varName}, and variable {varName} joined with {value}"

- Then
    - "Variables {varName} and {varName} are equal"
    - "Variable {varName} is equal to {value}"     

-----
To use a test build of SquashDataSimulator

- install the dependencies
  ```bash
  pip install PyYAML argcomplete inquirerpy jsonschema
  ```
- install the test build
  ```bash
  pip install -i https://test.pypi.org/simple/ squash-data-simulator==0.1.6.32279
  ```
- create a YAML file `foo.yml`
  ```YAML
  TECHNOLOGY: Cucumber 5+
  EXTRA_OPTIONS:
    pom_path: ./pom.xml
  ENVS:
    FOO: 4242
  DATA:
    MyCampaign:
      MyIteration:
        MySuite:
          CUSTOM_FIELDS:
            suiteCUF: "###"
          TEST_CASES:
            param:
              TEST_FILE_PATH: src/test/resources/stringtest/squashParam.feature
              CUSTOM_FIELDS:
                C01: alpha👩‍❤️‍💋‍👨
                C02: beta
                C03: gam  ma
                C04: ""
              DATASET:
                COLUMNS: [1, 2, 3]
                français: [ un, deux, trois]
                anglais: [ one, two, three]
            var env:
              TEST_FILE_PATH: src/test/resources/stringtest/varenv.feature
              CUSTOM_FIELDS:
  ```
- use
  ```bash
  squash_data_simulator foo.yaml -t "Cucumber 5+" -m ./pom.xml single
  ```
  for Git bash
  ```bash
  winpty squash_data_simulator foo.yaml -t "Cucumber 5+" -m ./pom.xml single
  ```
  if `pom_path` is defined in `foo.yml`, use
  ```bash
  squash_data_simulator foo.yaml -t "Cucumber 5+" single
  ```
